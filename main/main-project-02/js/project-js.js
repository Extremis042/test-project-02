/**
 * @file: project-js.js
 * @author: Carlos Palomino , Cristiano Rossetti, Gabriele Battistata, Erik Barale
 * Purpose of file
 *
 * This JS file contains a list of functions and event listeners used to create
 * a ball game
*/

/********************************************************************
 *                      GLOBAL VARIABLES                            *
 ********************************************************************
 * - ballId : Rappresents the single ball ID                        *
 *                                                                  *
 * - animations: Rappresents the list  of the current animations    *
 *                                                                  *
 * - timeThen/Now : Rappresents the time taken during the phases of *
 *                  the animations (then = when the user press the  *
 *                  mouse, now= when the user stop pressing the     *
 *                  mouse)                                          *
 *                                                                  *
 * - gravity : Rappresents a value used to simulate the gravity     *
 *                                                                  *
 * - xpos/ypos : Rappresents the x and y coordinates used to        *
 *               memorize the position of the mouse at the moment   *
 *               on the screen.                                     *
 *                                                                  *
 * - globalStop: This is a global variable which is used to stop    *
 *               and resume the animation of the ball based on it's *
 *               value                                              *
 *                                                                  *
 * - globalSize: This is a global variable which is used to         * 
 *               rappresent the ball width at the moment            *
 *                                                                  *
 * - globalSpeed: This is a global variable which is used to        * 
 *               rappresent the speed of the ball at the moment     *
 *                                                                  *
 * - skin : This is a global variable that rappresents the skin     *
 *          that the user choose (the img)                          *
 *                                                                  *
 * - insideTarget : This is a global variable that is used to       *
 *                  check if the user is clicking inside the        *
 *                  playground area (true) or not (false)           *
 *                                                                  *
 * - singleAlert : This is a global variable that is used to make   *
 *                 sure that 1 alert message at a time is displayed *
 *                 (true = no messages are displayed)               *
 *                 (false = a message is currently displayed)       *
 ********************************************************************/
var ballId = 0;
var animations = [];
var timeThen, timeNow;
var gravity = 0.02;
var xpos, ypos;
var globalStop;
var globalSize = 30;
var globalSpeed = 0;
var skin;
var insideTarget = false;
var singleAlert = true;

/**
 * Returns the sum of num1 and num2 
 * @param {Number} num1 - the first number
 * @param {Number} num2 - the second number 
 * @returns {Number} Sum of num1 and num2 
*/


/******************************************************
 *                     FUNCTIONS                      *
 ******************************************************/
/**
 * Creates the ball in the DOM when the user clicks the mouse 
 * @param {any} event - The click event
 * @param {number} xPosition - The X coordinates of the mouse
 *                             that will be added to the mouse position
 * @param {number} yPosition - The Y coordinates of the mouse
 *                             that will be added to the mouse position
 * @param {number} ballDimension - The dimension that the ball will have
 * @param {string} ballColor - The color that the ball will have
*/
function createBall(event, xPosition, yPosition, ballDimension, ballColor) {
    // Create a new element in the DOM (<span>) rappresenting the ball
    var ball = document.createElement("span");

    // Define it's dimension
    ball.style.width = ballDimension + "px";
    ball.style.height = ballDimension + "px";

    // And check if the ball has a skin (image) setted
    if (skin === undefined) {
        // If not, use its color
        ball.style.backgroundColor = ballColor;
    } else {
        // Otherwise, create a new <img> HTMLElement with the same shape and dimensions of the ball
        var img = document.createElement("img");
        img.style.borderRadius = 50 + "%";
        img.style.width = ballDimension + "px";
        img.style.height = ballDimension + "px";

        // And assign it's source from the skin global variable (from the file chooser)
        img.src = skin

        // And submit the changes inside the ball
        ball.appendChild(img);
    }

    // .. And define it's class
    ball.classList.add("ball");

    // Assign the values to the ball and add an ID
    ball.style.left = (xPosition - 15) + "px";
    ball.style.top = (yPosition - 15) + "px";
    ball.setAttribute("id", "ball" + ballId);

    // Adding the value of the ball id for the next creation
    ballId++;

    // Submitting all the changes
    document.body.appendChild(ball);
}


/**
 * Calculates some variables for the animation of the ball (velocity and direction)
 * and then calls the proper function that moves the ball
 * @param {number} pastXpos - the past X coordinate of the mouse (taken during the mousedown eventListener)
 * @param {number} pastYpos - the past Y coordinate of the mouse (taken during the mousedown eventListener)
 * @param {number} newXpos - the new X coordinate of the mouse (taken during the mouseup eventListener)
 * @param {number} newYpos - the new Y coordinate of the mouse (taken during the mouseup eventListener)
 * @param {object} ball - an HTMLSpanElement rappresenting the ball
 * @param {number} ballDimension - the ball dimension, used to check the right and lower edges in the
 *                                 animation
*/
function complexAnimation(pastXpos, pastYpos, newXpos, newYpos, ball, ballDimension) {
    /*
        CALCULATING VELOCITY
        First its calculated how many milliseconds are passed since the mousedown , 
        then the value its used as a dividend of a very big number in order to have
        very big numbers if the user is quick, otherwhise a progressive tiny number.

        Then to that number is addedd a number multiplied by globalspeed, which is calculated 
        subtracting the number of times the "decrease ball speed" button has ben been pressed
        to the number of times the "increase ball speed" button has been pressed

        The resulting number is then passed as an argument to the "avoidZero" function in order to avoid a velocity of zero
        which would break the application.

        "Change ball speed", the function that changes the values of globalSpeed has an upper and lower ceiling
        of 5 and -5 to the values that it can change since excessively high or low values for ball speed were found
        to be application-breaking.
 
    */
    var velocityFactor = ballSpeed(pastXpos, newXpos, pastYpos, newYpos, timeThen, timeNow);
    var velocity = avoidZero(Math.round((2500 / velocityFactor) + ((2500 / velocityFactor) * 0.15 * globalSpeed)));
    // Declaring a variable rappresenting the increasing force applied to the ball by gravity
    // In every frame this variable will increase
    var increasingGravity = 1;

    // Calculating the angular coefficient of the rect passing in the two points 
    var m = (pastYpos - newYpos) / (pastXpos - newXpos);    // eq: tan(angleOfTheRect)

    // Obtaining the angle rappresenting the direction the ball is going to take from the angular coefficent
    var ballDirection = Math.atan(m), directionFactor;


    // Check if the user is taking a direction to the left
    if (pastXpos > newXpos) {
        // In order to adjust the direction of the ball
        directionFactor = -1;
    } else {
        directionFactor = 1
    }

    // Assigning to the wheight variable the value of the global variable 
    let ballWheight = avoidZero(globalSize / 30);

    /**
     * A recursive function rappresenting the single frame of the animation of the ball
     * @returns {number} The unique id of the requestAnimationFrame() method.
    */
    function animationMotion() {
        // defining a variable rappresenting the gravity of the ball
        // The gravity will be greater every frame because of the increasing gravity variable
        let ballGravity = 1.001 * increasingGravity;

        // Assign to a variable the playground element
        let playgroundWidth = document.getElementById("playground-area");

        // And declare a variable rappresenting the upper limit that the ball will hit using the data from the playground
        let upperLimit=getComputedStyle(playgroundWidth).top.slice(0,-2) * 1;

        // Declare a variable rappresenting the lower limit
        let lowerLimit = upperLimit + playgroundWidth.offsetHeight*1;
        lowerLimit+=4;
        // Assign to a variable the value of the margin left of the playground
        let leftCorner = getComputedStyle(playgroundWidth).marginLeft.slice(0, -2);

        // Checking if the global variable that stops the animation is different from 1 (the user hasn't stopped the movement of the ball)
        // And if the velocity  of the ball declared before isn't negative in this frame
        if (globalStop !== 1 && velocity > 0) {
            // If yes, add to the X and Y coordinates the value of sin() and cos() of the direction angle calculed before
            ball.style.left = (ball.style.left.slice(0, -2) * 1 + directionFactor * Math.cos(ballDirection) * velocity / ballWheight) + "px";
            ball.style.top = (ball.style.top.slice(0, -2) * 1 + directionFactor * Math.sin(ballDirection) * velocity * ballWheight) + "px";
            // And assign it to the local variables ballX and ballY to shorten the code
            var ballX = ball.style.left.slice(0, -2) * 1;
            var ballY = ball.style.top.slice(0, -2) * 1;
            // CHECKING IF THE BALL HAS HIT ONE OF THE EDGES OF THE CONTAINER
            // If the ball has touched one of the edges of the container (left)
            if (ballX + directionFactor * Math.cos(ballDirection) * velocity < leftCorner) {
                // Then invert it's direction
                ballDirection = Math.atan2(Math.sin(ballDirection), Math.cos(ballDirection) * -1);
                // Then check if the velocity variable would be negative before subtracting to it
                if (velocity - 1 > 0) {
                    // If not, subtract the value to slow the velocity of the ball
                    velocity = velocity - 1;
                } else {
                    // Otherwise, set it to 0 so that the animation will be stopped
                    velocity = 0;
                }
                // And then, set a default position so that the scrollbars will not appear
                ball.style.left = getComputedStyle(playgroundWidth).marginLeft.slice(0, -1) * window.innerWidth / 100 + "px";
            } else if (ballX + ballDimension + (directionFactor * Math.cos(ballDirection) * velocity) > getComputedStyle(playgroundWidth).marginLeft.slice(0, -2) * 1 + getComputedStyle(playgroundWidth).width.slice(0, -2) * 1) {
                // Otherwise if the ball is hitting the right edge do the same
                ballDirection = Math.atan2(Math.sin(ballDirection), Math.cos(ballDirection) * -1);
                // Then check if the velocity variable would be negative before subtracting to it
                if (velocity - 1 > 0) {
                    // If not, subtract the value to slow the velocity of the ball
                    velocity = velocity - 1;
                } else {
                    // Otherwise, set it to 0 so that the animation will be stopped
                    velocity = 0;
                }
                // And then, set a default position so that the scrollbars will not appear
                ball.style.left = getComputedStyle(playgroundWidth).marginLeft.slice(0, -2) * 1 + getComputedStyle(playgroundWidth).width.slice(0, -2) * 1 - (ballDimension + 5) + "px";
            }
            // If the ball has touched the upper limit
            if (ballY < upperLimit) {
                // Then invert it's direction
                ballDirection = Math.atan2(Math.sin(ballDirection) * -1, Math.cos(ballDirection));
                // Then check if the velocity variable would be negative before subtracting to it
                if (velocity - 5 > 0) {
                    // If not, subtract the value to slow the velocity of the ball
                    velocity = velocity - 5;
                } else {
                    // Otherwise, set it to 0 so that the animation will be stopped
                    velocity = 0;
                }
                // And then, set a default position so that the scrollbars will not appear
                ball.style.top = upperLimit + "px";
            } else if (ballY + (ballDimension + 1) + Math.abs(Math.sin(ballDirection) * velocity) > lowerLimit) {
                // Otherwise if the ball is hitting the lower limit then do the same
                ballDirection = Math.atan2(Math.sin(ballDirection) * -1, Math.cos(ballDirection));
                // And modify the ball position so that the ball won't go out of the bounds of the playground
                ball.style.top = (lowerLimit - (ballDimension + 2)) + "px";
                // Reset the gravity so that the ball will actually bounce
                increasingGravity = 0;
                // And decrease it's velocity if possible
                if (velocity - 5 > 0) {
                    velocity = velocity - 5;
                } else {
                    velocity = 0;
                }
            } else {
                // Check if the next Y move of the ball won't touch the lower border or exeed it
                if (ballY + (ballDimension + 1) + Math.abs(Math.sin(ballDirection) * velocity) + ballGravity < lowerLimit) {
                    // Add some additional value to the Y in order to rappresent gravity 
                    ball.style.top = (ball.style.top.slice(0, -2) * 1 + ballGravity) + "px";
                    // And increase it for the next iteration
                    increasingGravity++;
                } else {
                    // Otherwise reset the value of the gravity in order to make the ball bounce without external forces
                    increasingGravity = 0;
                    // But reduce it's velocity every bounce to the ground (if it is greater than 0)
                    if (velocity - 1 > 0) {
                        velocity--;
                    } else {
                        velocity = 0;
                    }
                    // Also reset it's position to prevent the ball to exceed the limit
                    ball.style.top = (lowerLimit - (ballDimension + 4)) + "px";
                }
            }
        }

        // Otherwise check if the ball is stil
        if (velocity == 0) {
            // And check if the ball is outside of the playground, if yes, reposition it
            if (ball.style.left.slice(0, -2) * 1 < leftCorner) {
                //alert("a");
                ball.style.left = leftCorner + ballDimension +"px";
            } else if (ball.style.left.slice(0, -2) * 1 + globalSize > getComputedStyle(playgroundWidth).width.slice(0, -2)*1 + getComputedStyle(playgroundWidth).marginLeft.slice(0, -2) *1 ) {
                
                ball.style.left = getComputedStyle(playgroundWidth).width.slice(0, -2) * 1 - (ballDimension + 5) + "px";
            }else{
                // If yes, set it's position to the ground
                ball.style.top = (lowerLimit - (ballDimension +4)) + "px";
            }
        }
        // Return the ID of the animation to be memorized in the array of animations
        return window.requestAnimationFrame(animationMotion);
    }
    // Lastly, assing to the array of animations the value returned by the animation function calling it.
    animations[ballId - 1] = animationMotion();
}

/**
 * Changes the value of the global variable 'globalStop' in order to stop the motion of the balls
*/
function stopAnimation() {
    // First, check if at least 1 animation is going and if the global variable of stop has the value of 1
    if (animations[0] !== undefined) {
        if (globalStop == 1) {
            globalStop = 0;   // if yes, change it to 0
            document.getElementsByClassName("btn")[0].setAttribute("class", "btn play");
        } else {
            globalStop = 1;   // otherwise change it to 1
            document.getElementsByClassName("btn")[0].setAttribute("class", "btn pause");
        }
    } else {
        globalStop = 0;
        document.getElementsByClassName("btn")[0].setAttribute("class", "btn play");
    }
}

/**
 * Stops the animation of the last ball and then delete it
 */
function deleteLastBall() {
    // Check if the global variable rappresenting the last id of the ball is greater than 0 (if the user has created at least 1 ball)
    if (ballId > 0) {
        // if yes,  stop the animation of the last ball created.
        window.cancelAnimationFrame(animations[ballId - 1]);

        // Then delete entirely the ball by using the remove child element for compatibility with IE.
        let ballToRemove = document.getElementById("ball" + (ballId - 1));
        ballToRemove.parentNode.removeChild(ballToRemove);
        

        // And delete the animation id from the array of animations
        animations.pop(ballId - 1);

        // And finally, subtract the value of the ids
        ballId -= 1;
    }
}

/**
 * Reset size, velocity and delete all balls from playground
 */
function reset() {
    //Change button class to start css animation
    document.getElementsByClassName("nomove")[0].setAttribute("class", "rotate");
    //After css animation the element come back with his original class
    setTimeout(function () {
        document.getElementsByClassName("rotate")[0].setAttribute("class", "nomove");
    }, 800);
    //Delete the ball using deleteLastBall() function for all balls
    while (ballId > 0) {
        deleteLastBall();
    }
    //Setting size and velocity at default values
    globalSize = 30;
    globalSpeed = 0;
    //Reset play/pause button using stopAnimation() function
    stopAnimation();
}

/**
 * Change the size of the ball by a certain amount
 * @param {number} amount - The given amount (can be negative)
 */
function changeBallSize(amount) {
    // If the ball has a size of at least 30 (the min val) or if its value will be less than 80(max), change the global variable
    globalSize + amount >= 30 && globalSize + amount < 85 ? globalSize += amount : globalSize;
}

/**
 * Changes the ball speed by a certain amount (change)
 * @param {number} change The certain amount of change
 */
function changeBallSpeed(change) {
    // Change the value of the globalSpeed by a certain amount
    globalSpeed += change;
    // Then, check if the speed is greater or equal of 5
    if (globalSpeed >= 5) {
        sendAlert("You have reached the max speed possible!");
        // And set the globalSpeed of 5
        globalSpeed = 5;
        // Otherwise, check if it is lower or equal to -5
    } else if (globalSpeed <= -5) {
        sendAlert("You have reached the min speed possible!");
        globalSpeed = -5;
    }
}

/**
 * Send an alert that will be displayed on screen
 * @param {string} message - The message that will be displayed
*/
function sendAlert(message) {
    // Check if no messages are currently displayed
    if (singleAlert) {
        // If yes, get the alert message element , define it's message and declare a variable rappresenting the current opacity
        let alertElmnt = document.getElementById("popup-message"), currentOpacity = 0.0;
        alertElmnt.innerText = message;

        // Then declare a variable that will contain the id of the animation of pop up
        let popUpAnimationID;

        // And declare the proper animation function
        let popAlert = function () {
            // Check if the animation has not is max value
            if (alertElmnt.style.opacity < 1.0) {
                // If it hasn't, add some value to the current opacity and continue the animation
                alertElmnt.style.opacity = (currentOpacity += 0.05) + "";
                popUpAnimationID = window.requestAnimationFrame(popAlert);
            } else {
                // Otherwise, cancel the animation and wait some time before continuing
                window.cancelAnimationFrame(popUpAnimationID);

                setTimeout(function () {
                    // Then, redeclare the current opacity and declare another function that will
                    // make desappear the alert
                    currentOpacity = 1.0;
                    let popClose = function () {
                        // If the alert hasn't already desappeared
                        if (currentOpacity > 0.0) {
                            // Reduce it's opacity and continue the animation
                            alertElmnt.style.opacity = (currentOpacity -= 0.05) + "";
                            popUpAnimationID = window.requestAnimationFrame(popClose);
                        } else {
                            // Otherwise cancel the animation and make possible to show new alerts
                            // by setting the singleAlert variable to true
                            window.cancelAnimationFrame(popUpAnimationID);
                            singleAlert = true;
                        }
                    }
                    popClose();
                }, 5000);
            }
        }
        popAlert();
        // After the start of the animation, set the variable to false in order to avoid overlapping alerts
        singleAlert = false;
    }
}


/**
 * It calculates the speed of the ball by taking into account the distance between the mousedown and mouseup events
 * both in space (with the "oldX,newX,oldY,newY" set of coordinates), and time(with the "timeDown,TimeUp" set of coordinates).
 * 
 * It then assigns a speed variation to the ball making it slower or faster.
 * The logic of the function is :
 * 
 * Considering an equal unit of time, Velocity increases with the increase of distance, and since 
 * the value that this function returns then becomes a variable (VelocityFactor), which is inversely 
 * proportional to Velocity, the greater the mouse movement the smaller the distMultiplier that multiplicates the equal unit of time,
 * resulting in a smaller speedFactor and thus in a bigger velocity.
 * @param {number} oldX - The X position of the mouse when the user press the left click button
 * @param {number} newX - The X position of the mouse when the user releases the pression of the left button 
 * @param {number} oldY - The Y position of the mouse when the user press the left click button
 * @param {number} newY - The Y position of the mouse when the user releases the pression of the left button
 * @param {number} timeDown - The time taken when the user press the left button
 * @param {number} timeUp - The time taken when the user releases the press on the left button
 * @returns The effective speed of the ball based on it's time and space
 */
function ballSpeed(oldX, newX, oldY, newY, timeDown, timeUp) {
    var distMultiplier;
    var Spacedistance = Math.sqrt(Math.pow((newX - oldX), 2) + Math.pow((newY - oldY), 2));
    var Timedistance = timeUp - timeDown;

    // Checking the value inside the Spacedistance and for each result, change the value of the distMultiplier
    if (Spacedistance >= 1000) {
        distMultiplier = 0.40;
    } else if (Spacedistance < 1000 && Spacedistance >= 700) {
        distMultiplier = 0.50
    } else if (Spacedistance < 1000 && Spacedistance >= 700) {
        distMultiplier = 0.50
    } else if (Spacedistance < 700 && Spacedistance >= 500) {
        distMultiplier = 0.75
    } else if (Spacedistance < 500 && Spacedistance >= 350) {
        distMultiplier = 1
    } else if (Spacedistance < 350 && Spacedistance >= 250) {
        distMultiplier = 1.5
    } else if (Spacedistance < 250 && Spacedistance >= 150) {
        distMultiplier = 2
    } else if (Spacedistance < 150) {
        distMultiplier = 2.5
    }
    // Then multiply the multiplier to the difference of time and return it
    return Timedistance * distMultiplier;
}

/**
 * Avoids the 0 value by checking if it's value is greater or equal than 1
 * @param {number} speedNum - The speed value (number) passed to this function
 * @returns The same value if it's greater or equal than 1, 1 otherwise
 */
function avoidZero(speedNum) {
    if (speedNum <= 1) {
        return 1;
    } else {
        return speedNum;
    }
}

/**
 * Updates the dimensions of the body and the dimensions of the playground as well as the ball counter
*/
function updateScreen() {
    // Matches the height of the body with the height of the window
    document.body.style.height = window.innerHeight + "px";
    // Then declares a variable equal to the HTML element with the id 'playground-area'
    var playground = document.getElementById("playground-area");
    // Then, display the current number of balls, percentage of speed and ball size by displaying global variables
    document.querySelector("#ball-counter").innerHTML = "<strong>Balls: </strong>" + ballId;
    //Direct conversion of globalspeed in percentage
    document.querySelector("#velocity-counter").innerHTML = "<strong>Speed: </strong>" + (globalSpeed + 5) * 10 + "%";
    //Coversion of size in percentage by comparsion of global variable to an array which contains all possible globalSize values
    var percSize = [];
    for (let i = 30; i <= 80; i++)
        if (i % 5 == 0)
            percSize.push(i);
    document.querySelector("#size-counter").innerHTML = "<strong>Size: </strong>" + (globalSize == 30 ? "default" : " + " + (percSize.indexOf(globalSize)) * 10 + "%");
    // And repeats itself by calling the requestAnimationFrame
    window.requestAnimationFrame(updateScreen);
}
// Start the loop
updateScreen();



/******************************************************
 *                  EVENT LISTENERS                   *
 ******************************************************/

// Adding an event listener to "grab" the choosen img from the user when he clicks the button to choose the file
document.getElementById('ball-skin').onchange = function (chooseEvent) {
    // Declaring a variable equals to the target element of the event or it's source element if
    // it doesn't support the target
    var targetElement = chooseEvent.target || window.event.srcElement,
        files = targetElement.files;     // And a files variable equals to the files of the target element

    // Checking if the browser has the FileReader and files of the target support
    if (FileReader && files && files.length) {
        // If yes, create a new object called readerFile
        var readerFile = new FileReader();

        // And call a function when the object loads data
        readerFile.onload = function () {
            // And assing it's result to the skin global variable
            skin = readerFile.result;
        }
        // Then, read the data from the files variable
        readerFile.readAsDataURL(files[0]);
    }
}

// Adding an event listener to the whole page that listens for a click
window.addEventListener('mousedown', function (mouseDownEvt) {
    // Checking if the user has clicked the mouse inside the playground area
    if (mouseDownEvt.target.id == "playground-area") {

        // If yes, then take the curren mouse coordinates
        // Checking if the mouse event is supported, if yes take it with it, otherwise take it with window.event
        if(MouseEvent){
            xpos = mouseDownEvt.clientX;
            ypos = mouseDownEvt.clientY;
        }else{
            xpos = window.event.clientX;
            ypos = window.event.clientY;
        }
        
        // Take the seconds to measure the velocity and change the result of the global variable insideTarget to true
        timeThen = Date.now();
        insideTarget = true;
    } else {
        insideTarget = false;
    }
});


// Adding another listener to confront the past and present positions (when the user pulls out the finger)
window.addEventListener('mouseup', function (moveEvent) {
    // Check if the global variable insideTarget is equals to true (if the user has clicked inside the playgroundArea)
    if (insideTarget) {
        // Take the time of now
        timeNow = Date.now();

        // Getting the new positions of the mouse
        
        // Checking if the mouse event is supported, if yes take it with it, otherwise take it with window.event
        if(MouseEvent){
            newXpos = moveEvent.clientX - 15;
            newYpos = moveEvent.clientY - 15;
        }else{
            newXpos = window.event.clientX - 15;
            newYpos = window.event.clientY - 15;
        }
    

        // Getting the previous position of the mouse through the position of the ball
        pastXpos = xpos - 15;
        pastYpos = ypos - 15;

        // Checking if the user is dragging the mouse by checking the past with the current coordinates
        if (pastXpos !== newXpos || pastYpos !== newYpos) {
            // If yes, take the dimension of the ball locally
            let ballDimension = globalSize;

            // then take it's color from the color chooser 
            var ballColor = document.getElementById("ball-color").value;

            // and then create the ball
            createBall(moveEvent, xpos, ypos, ballDimension, ballColor);

            // Getting the last ball created using the last ID created and its id
            ball = document.getElementById("ball" + (ballId - 1));

            // Calling the function that creates the animation of motion of the balls
            complexAnimation(pastXpos, pastYpos, newXpos, newYpos, ball, ballDimension);
        }
    }
});

// Adding another listener to change the size and the speed of the next ball using the keyboard keys
window.addEventListener('keydown', function (keyEvent) {
    // If the the key pressed is an A subtract 5 size
    if (keyEvent.key === 'a') {
        changeBallSize(-5);
    } else if (keyEvent.key === 'd') {
        // otherwise if it is a D add 5 size to the ball
        changeBallSize(5);
    } else if (keyEvent.key === '-') {
        // otherwise if it is a D add 1 speed to the ball
        changeBallSpeed(-1);
    } else if (keyEvent.key === '+') {
        // otherwise if it is a D add 1 speed to the ball
        changeBallSpeed(1);
    }
});

// Adding an event listener that will force the color choonsen by the user if a image is used
document.getElementById("ball-color").addEventListener('input', function () {
    skin = undefined;
});

// Adding an event listener for the mobile version that handles the touchstart event
window.addEventListener('touchstart', function (touchDownEvt) {
    // Checking if the user has clicked the mouse inside the playground area
    if (touchDownEvt.target.id == "playground-area") {
        // If yes, then take the curren mouse coordinates
        xpos = touchDownEvt.touches[0].clientX;
        ypos = touchDownEvt.touches[0].clientY;
        // Take the seconds to measure the velocity and change the result of the global variable insideTarget to true
        timeThen = Date.now();
        insideTarget = true;
    } else {
        insideTarget = false;
    }
});

// Adding an event listener for the mobile versione that handles the touchend event
window.addEventListener('touchend', function (touchUpEvt) {
    // Check if the global variable insideTarget is equals to true (if the user has clicked inside the playgroundArea)
    if (insideTarget) {
        // Take the time of now
        timeNow = Date.now();

        // Getting the new positions of the mouse
        newXpos = touchUpEvt.changedTouches[0].clientX - 15;
        newYpos = touchUpEvt.changedTouches[0].clientY - 15;

        // Getting the previous position of the mouse through the position of the ball
        pastXpos = xpos - 15;
        pastYpos = ypos - 15;

        // Checking if the user is dragging the mouse by checking the past with the current coordinates
        if (pastXpos !== newXpos || pastYpos !== newYpos) {
            // If yes, take the dimension of the ball locally
            let ballDimension = globalSize;

            // then take it's color from the color chooser 
            var ballColor = document.getElementById("ball-color").value;
            // and then create the ball
            createBall(touchUpEvt, xpos, ypos, ballDimension, ballColor);

            // Getting the last ball created using the last ID created and its id
            ball = document.getElementById("ball" + (ballId - 1));

            // Calling the function that creates the animation of motion of the balls
            complexAnimation(pastXpos, pastYpos, newXpos, newYpos, ball, ballDimension);
        }
    }
});

/**
 * This function prints the mouse coordinates when it moves
 * @param {object} event - The event of the moving mouse
 */
function printCoordinates(event) {
    var coordinates = document.getElementById("coordinates");
    coordinates.innerHTML = " <strong> X: </strong>" + event.clientX + "<strong>  Y: </strong>" + event.clientY;
}

// Align the height of the body with the height of the screen
document.body.style.height = window.innerHeight + "px";


// Adding a function to implement a color chooser to IE
document.body.onload = function(){
    // Getting the user agent
    var userAgent = window.navigator.userAgent;
    // And test if the pattern of IE is present (MSIE | Trident) with the help of the boolean function test
    var isIE = /MSIE|Trident/.test(userAgent);

    // IF the pattern is present
    if ( isIE ) {

        // Then, get the ball input
        let ballColor = document.getElementById("ball-color");

        // And create an object html element that will contain the color chooser trough it's classid
        let properColorChooser = document.createElement("object");
        properColorChooser.setAttribute("id" , "chooser");
        properColorChooser.setAttribute("classid","CLSID:3050F819-98B5-11CF-BB82-00AA00BDCE0B");

        // Make the color chooser invisible
        properColorChooser.style.width="0";
        properColorChooser.style.height="0";

        // And assign an eventlistener that will listen for an user click on the element
        ballColor.addEventListener('click' ,   function(){
            // And get the color choosen from the color selector using the ChooseColorDlg
            var finalColor = document.getElementById("chooser").ChooseColorDlg(ballColor.value);
            // And assign it to the value of the ball color so that it will translate the value to a valid color rappresentation
            window.event.srcElement.style.color = finalColor; 
            // And finally set the value attribute of the color from it's color attribute
            ballColor.setAttribute("value" ,  window.event.srcElement.style.color);
        });
        // And finally append the color chooser element to the DOM
        ballColor.parentElement.appendChild(properColorChooser);
    }
}