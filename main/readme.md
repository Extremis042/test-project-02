
***Power-Balls Project***
==================


Summary
--------------
- **Introduction** 

- **Exercise Requirements**

- **Project description**

- **Configuration and technical characteristics**

- **Usage(how to setup, run and use the application)**

- **Files and Project structure**

- **Authors: names, roles and team composition**

- **Features delivered**

- **External resources(Links and description of external resources such as JSON files, APIs, DBs, etc)**

- **Browser compatibility**

- **Licence and contact information**

- **Changelog and version history**



Introduction
---------------------
Create an application that displays interactive bouncing balls and a navigation panel to control their behavior.



Exercise Requirements
---------------------
Create an application that allows users to create and interact with bouncing balls, similar to examples demostrated in class and the included screenshots and with the following requirements:

- new balls should be generated when the user clicks and drags the mouse

- the speed of the balls should depend on the speed of the mouse movement

- the direction of the balls should depend on the direction of motion of the mouse

- the ball movement should bounce at the edges of the container

- scrollbars should not appear when the ball hit the edges of the container

- the ball movement should have some physicality( wight, gravity, speed)

- it should be possible to 'freeze' and 'resume' the animation 

- the users should be able to choose the 'skin' of the balls, colors, images, etc

- the users should be able to make the balls bigger or smaller with keyboard shortcut and onscreen buttons

- the users should be able to remove balls (e.g by clicking on them or on a button)

- the users should be able to increase/decrease the speed of balls with keyboard shortcuts and onscreen buttons

- the page should be page responsive, meaning that when the user changes the suze of the page, the balls never go out of their container and all page controls sre repositioned to fit the viewport

- add an onscreen counter that displays the current number of balls


Project description
------------------- 
<img src="md-images/animation.gif"
     alt="functioning demonstration"/><br>
Index.html
----

**Index :** 

The page is made of a few buttons which, when clicked, execute a series of different functions which we will now see in detail, a few interactive elements 
showing some informations, such as the position of the mouse, and the "playground", a canvas element in which balls are spawned and can bounce around.

**Basic Functioning :**

An event listener activates everytime the user pushes down the left mouse botton or touches the screen with his finger and stores the coordinates in two variables, it then does the same with the release of the mouse. It also stores the moment these two events happen.
The resulting six variables are then used to calculate the "vector" of the ball and it's speed.

As the mouse button isn't pressed anymore, a function generates the ball either with default values or with the values it gets from some event listeners for things like color and ball skin in the position the mouse was first pressed.

Another function might also send different values for velocity to the ball-generating function.

The ball is then subjected to gravity and limited in movement by the boundaries of the playground.

**On-screen coordinates :**<br>
<img src="md-images/mouse-coordinates.gif"
     alt="Mouse coordinates"/>
Everytime the mouse moves an event listener records it's position in two coordinates which are then passed to the function which changes the inner HTML of the div that contains the Coordinates so that it continuously updates itself.

**Ball-Spawning and deleting :**
<img src="md-images/spawn-delete.gif"
     alt="Spawn an delete"/><br>
Each time a ball is created a variable displaying the highest ballId present in the playground is used to display the number of existing balls.

This is possible since BallId is a property that all balls have, that starts from 0 and is increased by 1 each time a new ball is spawned.

When the user presses the "delete last ball" button a function deletes the ball with the highest BallId.The Id may then be re-used if a new ball is created afterwards.

The "reset" button simply keeps on calling this function until BallId stops being bigger than 0, and it then resets some global variable to their state at the start of the application.

**Changing the ball's speed :**
<img src="md-images/speed-control.gif"
     alt="Speed control"/><br>
Touching the "Increase ball speed" and "Decrease ball speed" buttons will accordingly increase or decrease a variable that for each click will increase or decrease the ball's speed by an amount equal to 15% of Base speed.

The variable will also be used to update a div that will tell us how much further we can
increase or decrease the speed.

There are an upper and lower ceiling of 5 increases and 5 decreases.

**Changing the ball's size :**
<img src="md-images/size-control.gif"
     alt="Size control"/><br>
Touching the "Increase ball size" and "Decrease ball size" buttons will accordingly increase or decrease the variable that dictates the size of any new ball spawned.

Each click will increase or decrease the ball's size by an amount equal to 1/6 of Base size.

The variable will also be used to update a div that will tell us how much further we can increase or decrease the size.

There is an upper ceiling of 10 increases, the minimum size is already the default value.

**Changing the ball's skin :**
<img src="md-images/skin-change.gif"
     alt="skin change"/><br>
To change the ball's skin to a custom one, you have to submit an image of your choosing trough the "change ball skin with an image" button, which is actually a file button that accepts files in the .jpg, .png, .jpeg formats.

The event Listener that handles setting the ball's attributes before creation will then get the image you chose and will substitute it to the default skin.

**Changing the ball's color :**
<img src="md-images/color-change.gif"
     alt="Color change"/><br>
To change the ball's color you have to click the "change ball color" button and then choose it from the color picker that will appear on it's side. 

The event Listener that handles setting the ball's attributes before creation will then get the color you chose and will pass it to the function that creates the ball, which will use it as the background-color for that and any following ball, until you change it again.

In case no color has been specified the function will default to black. 

**Starting and stopping the animation**
<img src="md-images/start-stop.gif"
     alt="Start and stop"/><br>
What this button does is changing the value of a global variable, GlobalStop, from 'undefined' to 1 at first, and then from 1 to 0 and viceversa everytime it is used afterwards.

Since the variable's value is checked by the function that handles the animation and a value of 1 will block the loop, the animation stops on first pressure and everytime the value is reset to 1, only to resume with another pressure, which will set to 0.

**Reset animation at default state**
<img src="md-images/reset.gif"
     alt="Reset"/><br>
This button will invoke reset() function in js which take to default all main variabiles such as globalSpeed, globalSize, etc.
It also take the play/pause button in original 'play' state.

Configuration and technical characteristics
-------------------
This project consists of a singular web-page, which was developed keeping in mind the principles of content-first and mobile-first and is thus responsive and fully and comfortably usable from most existing devices.

This can be also deduced from the simple user UI, which almost doesn't use text and, for it's main functionalities, is made of intuitive icons.
Where text appears, it is also very short and it uses a very big font.


Usage(how to setup, run and use the application)
-------------------
The setup is simple, just doubleclick the index.html file found in the first directory or copy all files and folders inside 'main-project-02' inside your web server htdocs folder and with any supported browser go to http://localhost and ejoy our application!


Files and Project structure
-------------------
The following is the project structure:

- main
    - readme.md
        - md-images
            - (readme images and gif)
    - main-project-02
        - index.html
        - images
            - (page icons and images)
        - js
            - project-js.js
        - style
            - project-style.css
        - jsdoc
            - index.html
            - global.html
            - project-js.js.html
            - fonts
               - (jsdoc font files)
            - scripts
               - (jsdoc js files)
            - styles
               - (jsdoc css files)

Authors: names, roles and team composition
-------------------
**Developers name** : Carlos Palomino, Gabriele Battistata, Cristiano Rossetti, Erik Barale.

Features delivered
-------------------
- Resizing balls
- Changing balls speed
- Changing balls color
- Changing balls skin with an image
- Keyboard shortcuts
- Full responsivne
- Touchscreen support
- Main browser support

External resources(Links and description of external resources such as JSON files, APIs, DBs, etc)
-------------------
We didn't use any external resources for this project, it's built in house.

Browser compatibility
-------------------

**Chromium-based** : Fully supported
**Opera** : Fully supported
**Firefox** : Fully supported
**Edge** : Fully supported
**Vivaldi** : Fully supported
**Safari** : Fully supported
**IE 11** : Partially supported*

Evolution of layout
-------------------
First drawing make with Adobe XD
<img src="md-images/first-layout.png"
     alt="First layout project"/>
Giving a little style on Adobe XD
<img src="md-images/second-layout.png"
     alt="Second layout project"/>
Final result on browser
<img src="md-images/final-layout.png"
     alt="Final layout"/>

*All main features work properly and the page does get not stuck, but some features has partial compatibility or they are completely absent.

Contact information
-------------------
Personal E-mail:

- Erik Barale: erik.barale@edu.itspiemonte.it

- Gabriele Battistata: gabriele.battistata@edu.itspiemonte.it

- Carlos Palomino: carlos.palomino@edu.itspiemonte.it

- Cristiano Rossetti: cristiano.rossetti@edu.itspiemonte.it


Changelog and version history
-------------------
1# Project Started, ball creation
2# Starting to animate, the ball follows the mouse movement when launched
3# The ball reaches all the container borders, bouncing around
4# Added the possibility of increasing the size of the ball
5# Added the possibility to change the skin of the ball with an image
6# Fixed the phisics of the ball; code cleaning and added some comments to the JS file
7# Added the possibility of increasing or decreasing the speed of the ball
8# Fixed the issue with the speed of the ball
9# Added a 'weight' to the ball; the ball now could be created only inside the 'playground-area'.
10# Added dynamics buttons (ball speed), changed the 'velocityFactor'
11# Added keyboards shortcuts to increase/decrease the ball speed and it's size 
12# JS doc compleated and cleaned the animation functions
13# Added a side menu for the different contols; added a style to the page
14# Added a play/pause and a reset animated button; 
14# Removed debug alerts 
15# Fixed the overall responsiveness of the balls when resizing the canvas
16# Progect is 99% complete, Added the title and ready to the release version v1.0